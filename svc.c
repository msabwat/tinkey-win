
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(default:4668)
#include <wchar.h>
#include <tchar.h>
#include <winsvc.h>
#include <stdio.h>


#define SERVICE_NAME _T("Tinkey")

SERVICE_STATUS        g_ServiceStatus = {0};
SERVICE_STATUS_HANDLE g_StatusHandle = NULL;
HANDLE                g_ServiceStopEvent = INVALID_HANDLE_VALUE;

VOID ServiceMain(DWORD argc, LPTSTR *argv);
VOID ServiceCtrlHandler(DWORD);
DWORD ServiceWorkerThread(LPVOID lpParam);

int _tmain(int argc, _TCHAR* argv[])
{
    const _TCHAR* start_cmd = _T("start");
    const _TCHAR* install_cmd = _T("install");
    const _TCHAR* stop_cmd = _T("stop");
    const _TCHAR* delete_cmd = _T("delete");
    if (argc != 2 || argv[1] == NULL)
        return EXIT_FAILURE;

    if (_tcscmp(argv[1], install_cmd) == 0)
        _tprintf(_T("calling install \n"));
    else if (_tcscmp(argv[1], start_cmd) == 0)
        _tprintf(_T("calling start \n"));
    else if (_tcscmp(argv[1], stop_cmd) == 0)
        _tprintf(_T("calling stop \n"));
    else if (_tcscmp(argv[1], delete_cmd) == 0)
        _tprintf(_T("calling delete \n"));
    else
        _tprintf(_T("unknow command %s\n"), argv[1]);
    
    /*
    SERVICE_TABLE_ENTRY ServiceTable
    [] =
    {
        {SERVICE_NAME, (LPSERVICE_MAIN_FUNCTION)ServiceMain},
        {NULL, NULL}
    };

    if (StartServiceCtrlDispatcher(ServiceTable) == FALSE)
    {
        return GetLastError();
    }
    */
    return 0;
}
/*
VOID ServiceMain(DWORD argc, LPTSTR *argv)
{
    DWORD Status = E_FAIL;

    g_StatusHandle = RegisterServiceCtrlHandler(SERVICE_NAME, ServiceCtrlHandler);

    if (g_StatusHandle == NULL)
    {
        return;
    }

    ZeroMemory(&g_ServiceStatus, sizeof(g_ServiceStatus));
    g_ServiceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    g_ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    g_ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
    g_ServiceStatus.dwWin32ExitCode = 0;
    g_ServiceStatus.dwServiceSpecificExitCode = 0;
    g_ServiceStatus.dwCheckPoint = 0;

    SetServiceStatus(g_StatusHandle, &g_ServiceStatus);

    g_ServiceStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    if (g_ServiceStopEvent == NULL)
    {
        g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
        g_ServiceStatus.dwWin32ExitCode = GetLastError();
        SetServiceStatus(g_StatusHandle, &g_ServiceStatus);
        return;
    }

    g_ServiceStatus.dwCurrentState = SERVICE_RUNNING;
    SetServiceStatus(g_StatusHandle, &g_ServiceStatus);

    HANDLE hThread = CreateThread(NULL, 0, ServiceWorkerThread, NULL, 0, NULL);

    WaitForSingleObject(hThread, INFINITE);

    CloseHandle(g_ServiceStopEvent);

    g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
    g_ServiceStatus.dwWin32ExitCode = 0;
    SetServiceStatus(g_StatusHandle, &g_ServiceStatus);

    return;
}

VOID ServiceCtrlHandler(DWORD CtrlCode)
{
    switch (CtrlCode)
    {
    case SERVICE_CONTROL_STOP:
        if (g_ServiceStatus.dwCurrentState != SERVICE_RUNNING)
            break;

        g_ServiceStatus.dwControlsAccepted = 0;
        g_ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
        g_ServiceStatus.dwWin32ExitCode = 0;
        g_ServiceStatus.dwCheckPoint = 4;

        SetServiceStatus(g_StatusHandle, &g_ServiceStatus);

        SetEvent(g_ServiceStopEvent);

        return;

    default:
        break;
    }

    SetServiceStatus(g_StatusHandle, &g_ServiceStatus);

    return;
}

DWORD ServiceWorkerThread(LPVOID lpParam)
{
    while (WaitForSingleObject(g_ServiceStopEvent, 0) != WAIT_OBJECT_0)
    {
        // Perform main service function here

        // For example, printing "Hello, World!" to the console
        _tprintf(_T("Hello, World!\n"));

        Sleep(1000);  // Simulate some work
    }

    return ERROR_SUCCESS;
}
*/