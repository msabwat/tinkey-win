cc      =  cl
link    =  link
conlflags   =  /INCREMENTAL:YES /NOLOGO
svccflags  =  /Wall /WX /c /EHsc /MT /I "%WindowsSdkDir%\Include\um"
dependencies_lib = advapi32.lib

all: svc.exe

svc.exe: svc.obj
    $(link) $(ldebug) $(conflags) -out:svc.exe svc.obj $(dependencies_lib)

svc.obj: svc.c
    $(cc) $(cdebug) $(cflags) $(cvars) $(svccflags) svc.c

clean:
    del svc.obj svc.exe

rebuild: clean all